package com.javabot.aws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@SpringBootApplication
@RestController
@RequestMapping("/jb")
public class AwsSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(AwsSampleApplication.class, args);
	}

	@GetMapping("/sayHello")
	public Mono<String> sayHello(){
		return Mono.just("hello world");
	}
}
